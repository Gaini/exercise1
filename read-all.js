const fs = require('fs');
var files = [];

function ReadFile(path, name) {
    return new Promise(function (resolve, reject) {

        fs.readFile(path+'/'+name, 'utf8', function (err, content) {
            if (err)
                return reject(err);
            resolve({name, content});
        });
    })
}

function read(path) {
    return new Promise(function (resolve, reject) {

        fs.readdir(path, function (err, filenames) {
            if (err)
                return reject(err);
            resolve(filenames);
        });
    }).then(filenames => Promise.all(filenames.map(name => ReadFile(path, name)) ))
}

module.exports = read;