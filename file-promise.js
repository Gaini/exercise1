const fs = require('fs');

function read(file) {
    return new Promise(function (resolve, reject) {
        fs.readFile(file, 'utf8', function (err, data) {
            if (err)
                return reject(err)
            resolve(data);
        })
    })
}

function write(file, data) {
    return new Promise(function (resolve, reject) {
        fs.writeFile(file, data, function (err, data) {
            if (err)
                return reject(err)
            resolve(file);
        })
    })
}

module.exports.read = read;
module.exports.write = write;