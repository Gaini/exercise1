const fs = require('fs');

function InfoObject(path, type) {
    this.path = path;
    this.type = type;
}

function pathInfo(path, callback) {
    fs.stat(path, (err, stats) => {
        if (err)
            return callback(err, info);

        if (stats.isFile()) {
            var info = new InfoObject(path, "file");
            fs.readFile(path, 'utf8', function (err, contents) {
                if (err)
                    return callback(err, info);
                info.content = contents;
                return callback(null, info);
            });
        }

        if (stats.isDirectory()) {
            var info = new InfoObject(path, "directory");

            fs.readdir(path, (err, files) => {
                if (err)
                    return callback(err, info);
                info.childs = files;
                return callback(null, info);
            });
        }

    })
}

module.exports = pathInfo;